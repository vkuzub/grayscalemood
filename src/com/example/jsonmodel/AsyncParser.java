package com.example.jsonmodel;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.example.grayscalemood.core.TemporaryVariables;
import com.example.grayscalemoodtest.PlaylistActivity;
import com.example.grayscalemoodtest.R;
import com.google.gson.Gson;

public class AsyncParser extends AsyncTask<String, Void, Void> {

	private final String LOG_TAG = "myLogs";
	private Context context;
	private ProgressDialog dialog;
	private ListView playList;
	private TextView playlistName;

	public AsyncParser(Context context) {
		this.context = context;
	}

	public AsyncParser() {

	}

	@Override
	protected void onPreExecute() {
		if (context != null) {
			playlistName = (TextView)PlaylistActivity.getPlayListActivity().findViewById(R.id.activity_playlist_tvPlaylistName);
			playList = (ListView) PlaylistActivity.getPlayListActivity()
					.findViewById(R.id.activity_playlist_lv);
			dialog = new ProgressDialog(context);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setMessage(context.getResources().getString(
					R.string.please_wait));
			dialog.show();
		}
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(String... params) {
		long start = System.currentTimeMillis();
		Log.d(LOG_TAG, "AsyncTask Working");
		String url = params[0];
		String typeOfTag = params[1];

		InputStream source;
		Gson gson = new Gson();
		SearchResponse response = null;
		try {
			source = retrieveStream(url);
			Reader reader = new InputStreamReader(source);
			response = gson.fromJson(reader, SearchResponse.class);
		} catch (NullPointerException e) {
			Log.e(LOG_TAG, "NullPonterException");
			cancel(true);
		} catch (RuntimeException e) {
			Log.e(LOG_TAG, "RuntimeException");
			cancel(true);
		}

		if (typeOfTag.equals("moodactivity")) {

			Log.i(LOG_TAG, "moodactivity");

			ArrayList<ResultTags> results = response.results;
			ArrayList<String> mood = new ArrayList<String>();
			ArrayList<String> activity = new ArrayList<String>();
			for (ResultTags result : results) {
				if (result.type.equals("mood") && !mood.contains(result.value)) {
					mood.add(result.value);
					Collections.sort(mood);
				}
				if (result.type.equals("activity")
						&& !activity.contains(result.value)) {
					activity.add(result.value);
					Collections.sort(activity);
				}
			}
			new TemporaryVariables(activity, mood);
		}

		if (typeOfTag.equals("tracklist")) {
			ArrayList<ResultSongInfo> trackResults = response.trackList;
			ArrayList<String> tracklist = new ArrayList<String>();
			ArrayList<Map<String, String>> songInfo = new ArrayList<Map<String, String>>();
			for (ResultSongInfo resultSongInfo : trackResults) {
				tracklist.add(resultSongInfo.location);
				songInfo.add(putData(resultSongInfo.title,
						resultSongInfo.creator));
				Log.i(LOG_TAG, resultSongInfo.location);

			}
			new TemporaryVariables(tracklist, songInfo, null);
		}
		long end = System.currentTimeMillis();
		Log.d(LOG_TAG, String.valueOf(end - start));
		return null;

	}

	private HashMap<String, String> putData(String title, String creator) {
		HashMap<String, String> item = new HashMap<String, String>();
		item.put("title", title);
		item.put("creator", creator);
		return item;
	}

	protected void onPostExecute(Void result) {
		Log.d(LOG_TAG, "AsyncTask Stopped");

		if (context != null) {
			fillListAdapter();
			dialog.dismiss();
		}
		super.onPostExecute(null);
	}

	private void fillListAdapter() {
		playlistName.setText(TemporaryVariables.name);
		ArrayList<Map<String, String>> list = TemporaryVariables.song;
		String[] from = { "title", "creator" };
		int[] to = { android.R.id.text1, android.R.id.text2 };
		SimpleAdapter adapter = new SimpleAdapter(context, list,
				android.R.layout.simple_list_item_2, from, to);
		playList.setAdapter(adapter);
	}

	private InputStream retrieveStream(String url) {
		DefaultHttpClient client = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(url);
		try {
			HttpResponse getResponse = client.execute(getRequest);
			final int statusCode = getResponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				Log.w(getClass().getSimpleName(), "Error" + statusCode
						+ "for URL" + url);
				return null;
			}
			HttpEntity getresponseEntity = getResponse.getEntity();
			return getresponseEntity.getContent();
		} catch (Exception e) {
			getRequest.abort();
			Log.w(getClass().getSimpleName(), "Error for URL" + url, e);
			cancel(true);
		}
		return null;
	}

}