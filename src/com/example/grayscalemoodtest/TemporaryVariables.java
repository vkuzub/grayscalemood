package com.example.grayscalemoodtest;

import java.util.ArrayList;
import java.util.Map;

public class TemporaryVariables {

	public static ArrayList<String> mood;
	public static ArrayList<String> activity;

	public static ArrayList<String> tracklist;

	public static ArrayList<Map<String, String>> song;

	public static String name;
	public static String request;

	public static int position;
	
	public TemporaryVariables(ArrayList<String> activity, ArrayList<String> mood) {
		TemporaryVariables.mood = mood;
		TemporaryVariables.activity = activity;
	}

	public TemporaryVariables(ArrayList<String> tracklist,
			ArrayList<Map<String, String>> song, String noNecessary) {
		TemporaryVariables.tracklist = tracklist;
		TemporaryVariables.song = song;
	}

	public TemporaryVariables(String name, String request) {
		TemporaryVariables.name = name;
		TemporaryVariables.request = request;
	}

	public TemporaryVariables(int position) {
		TemporaryVariables.position = position;
	}
	
	public static void clear() {
		mood.clear();
		activity.clear();
		tracklist.clear();
		song.clear();
		name = null;
	}
	
	
}
