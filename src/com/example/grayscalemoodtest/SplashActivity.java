package com.example.grayscalemoodtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.widget.TextView;

import com.example.grayscalemood.core.NetworkConnectionChecker;
import com.example.jsonmodel.AsyncParser;

public class SplashActivity extends Activity {

	private final int DIALOG_EXIT = 1;
	private NetworkConnectionChecker ncc;
	private String jsonData = "https://docs.google.com/uc?export=download&id=0B7qyjoQoLzvfZkZmdjlSd09hcjQ";
	private Thread thread;
//	private String jsonData = "http://vk.com/doc13116462_131518040?hash=d2b414d05886b37db5&dl=ffe7c74ee8b02b426a";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().hide();
		}
		TextView splash = (TextView) findViewById(R.id.splash_tv_splashinfo);
		Typeface font = Typeface.createFromAsset(getAssets(), "myFontN.ttf");
		splash.setTypeface(font);
		TextView connecting = (TextView) findViewById(R.id.splash_tv_connecting);
		connecting.setTypeface(font);
		ncc = new NetworkConnectionChecker(this);
	}


	protected void onResume() {
		if (ncc.isNetworkAvailable()) {
			final AsyncParser parser = new AsyncParser();
			try {
				parser.execute(jsonData, "moodactivity");
			} catch (Exception e) {
				e.printStackTrace();
			}
			thread = new Thread(new Runnable() {
				public void run() {
					while (parser.getStatus()!=AsyncTask.Status.FINISHED) {
						//block
					}
					SplashActivity.this.finish();
			startActivity(new Intent(getApplicationContext(),
					MainActivity.class));
				}
			});
			thread.start();
		}
		else {
			extracted();
		}

		super.onResume();
	}

	@SuppressWarnings("deprecation")
	private void extracted() {
		showDialog(DIALOG_EXIT);
	}

	@Override
	public void onBackPressed() {
	}

	@SuppressWarnings("deprecation")
	@Override
	protected android.app.Dialog onCreateDialog(int id) {
		if (id == DIALOG_EXIT) {
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb.setMessage(getResources().getString(R.string.noconnect));
			adb.setPositiveButton(getResources().getString(R.string.exit), new OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					SplashActivity.this.finish();
				}
			});
			adb.setCancelable(false);
			return adb.create();
		}
		return super.onCreateDialog(id);
	}

}
