package com.example.grayscalemoodtest;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;

import com.example.grayscalemood.core.TemporaryVariables;
import com.example.grayscalemood.player.PlayerActivity;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {
	private final int DIALOG_EXIT = 1;
	static TabHost tabHost;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		tabHost = getTabHost();
		TabHost.TabSpec tabSpec;

		tabSpec = tabHost.newTabSpec("Mood");
		tabSpec.setIndicator("Mood");
		tabSpec.setContent(new Intent(this, MoodListActivity.class));
		tabHost.addTab(tabSpec);

		tabSpec = tabHost.newTabSpec("Activity");
		tabSpec.setIndicator("Activity");
		tabSpec.setContent(new Intent(this, ActivityListActivity.class));
		tabHost.addTab(tabSpec);

		tabSpec = tabHost.newTabSpec("Playlist");
		tabSpec.setIndicator("Playlist");
		tabSpec.setContent(new Intent(this, PlaylistActivity.class));
		tabHost.addTab(tabSpec);

		tabSpec = tabHost.newTabSpec("Player");
		tabSpec.setIndicator("Player");
		tabSpec.setContent(new Intent(this, PlayerActivity.class));
		tabHost.addTab(tabSpec);

		tabHost.setCurrentTabByTag("Mood");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_settings) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}

	@SuppressWarnings("deprecation")
	private void extracted() {
		showDialog(DIALOG_EXIT);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (TemporaryVariables.mood == null
				|| TemporaryVariables.activity == null) {
			extracted();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected android.app.Dialog onCreateDialog(int id) {
		if (id == DIALOG_EXIT) {
			AlertDialog.Builder adb = new AlertDialog.Builder(this);
			adb.setMessage(getResources().getString(R.string.notdownloaded));
			adb.setPositiveButton(getResources().getString(R.string.exit),
					new OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							MainActivity.this.finish();
						}
					});
			adb.setCancelable(false);
			return adb.create();
		}
		return super.onCreateDialog(id);
	}

	@Override
	public void onBackPressed() {

	}

}
