package com.example.grayscalemoodtest;

import com.example.grayscalemood.core.TemporaryVariables;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ActivityListActivity extends Activity {

	private ListView activityList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity_list);
		final String BASE_URL = "http://stereomood.com/";
		activityList = (ListView) findViewById(R.id.activity_activity_list_lv);
		activityList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String name = arg0.getItemAtPosition(arg2).toString();
				
				String request = BASE_URL + "activity" + "/" + name.replace(" ", "%20")
						+ "/playlist.json?shuffle&index=0";
				new TemporaryVariables(name,request);

				MainActivity.tabHost.setCurrentTabByTag("Playlist");

				// MainActivity.tabHost.setCurrentTabByTag("Player");
				// //tracklistposition Set!!!

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		fillAdapter();
	}
	
	private void fillAdapter(){
		ArrayAdapter<String> adapter;
		try {
			if (TemporaryVariables.activity != null) {
				 adapter = new ArrayAdapter<String>(
						this, android.R.layout.simple_list_item_1,
						TemporaryVariables.activity);
				activityList.setAdapter(adapter);
			} else {
				Log.e("myLogs", "NullPointer at ActivityListActivity");
			}

		} catch (NullPointerException e) {
			Log.e("myLogs", e.toString());
		}
	}
	
	public void onBackPressed() {
		//nothing
	}
	
}
