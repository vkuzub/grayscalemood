package com.example.grayscalemoodtest;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grayscalemood.core.TemporaryVariables;
import com.example.grayscalemood.player.PlayerActivity;
import com.example.jsonmodel.AsyncParser;

public class PlaylistActivity extends Activity {

	private ListView playList;
	private TextView playlistName;
	private AsyncParser parser = null;
	private static PlaylistActivity playListActivity;
	private String currRequest;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playlist);
		playListActivity = this;
		
		playList = (ListView) findViewById(R.id.activity_playlist_lv);

		playlistName = (TextView) findViewById(R.id.activity_playlist_tvPlaylistName);
		playlistName.setText("Select mood or activity");
		playList.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Log.i("myLogs", TemporaryVariables.tracklist.get(position));
				Log.i("myLogs", TemporaryVariables.song.get(position).values()
						.toString());
				Log.i("myLogs",
						TemporaryVariables.song.get(position).get("creator"));
				Log.i("myLogs",
						TemporaryVariables.song.get(position).get("title"));

				new TemporaryVariables(position);
				MainActivity.tabHost.setCurrentTabByTag("Player");
				PlayerActivity.setTrackListPosition(position);
				Log.i("myLogs",
						"in playlist position " + String.valueOf(position));
				// tracklistposition Set!!!
			}
		});
	}

	public static PlaylistActivity getPlayListActivity() {
		return playListActivity;
	}

	@Override
	protected void onResume() {
			
			if (currRequest!=TemporaryVariables.name) {
				currRequest=TemporaryVariables.name;
				startParsing();
				
			}
			
			
		super.onResume();
	}

	
	private void startParsing(){

				parser = new AsyncParser(this);
				parser.execute(TemporaryVariables.request, "tracklist");

	}
	
	private void fillListAdapter(){
		if (TemporaryVariables.song != null) {
			ArrayList<Map<String, String>> list = TemporaryVariables.song;
			
			String[] from = { "title", "creator" };
			int[] to = { android.R.id.text1, android.R.id.text2 };
			SimpleAdapter adapter = new SimpleAdapter(this, list,
					android.R.layout.simple_list_item_2, from, to);
			playList.setAdapter(adapter);
		} else {
			Log.e("myLogs", "NullPointer at PlaylistActivity");
			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.select_mood_activity),
					Toast.LENGTH_LONG).show();
		}
	}
	
	
	private void refreshAdapter() {
		// ��������� ���������� ?
	}

	public void onBackPressed() {
		// nothing
	}

}
