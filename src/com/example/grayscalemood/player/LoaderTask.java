package com.example.grayscalemood.player;

import com.example.grayscalemoodtest.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;

public class LoaderTask extends AsyncTask<String, Object, MediaPlayer> {

	MediaPlayer mp = new MediaPlayer();
	Context context;
	ProgressDialog dialog;

	public LoaderTask(Context context) {

	}

	@Override
	protected void onPreExecute() {
		Log.d("myLogs", "Atask working");
		if (context!=null) {
			dialog = new ProgressDialog(context);
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setMessage(context.getResources().getString(R.string.please_wait));
			dialog.show();
			}
		super.onPreExecute();
	}

	@Override
	protected MediaPlayer doInBackground(String... params) {
		long start = System.currentTimeMillis();
		try {
		    mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mp.setDataSource(params[0]);
			mp.prepare();
		} catch (Exception e) {
			Log.e("myLogs", "Exception in loaderTask");
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		Log.d("myLogs", String.valueOf(end - start));
		return mp;
	}

	@Override
	protected void onPostExecute(MediaPlayer result) {
		Log.d("myLogs", "Atask stopped");
		if (dialog!=null) {
			dialog.dismiss();
		}
		super.onPostExecute(result);
	}

}
