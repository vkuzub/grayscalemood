package com.example.grayscalemood.player;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.example.grayscalemood.core.TemporaryVariables;
import com.example.grayscalemoodtest.R;

public class PlayerActivity extends Activity implements OnClickListener,
		OnSeekBarChangeListener, OnCompletionListener,
		OnBufferingUpdateListener {

	private ImageButton play, prev, next;
	private TextView creator, title, atime, ptime;
	private SeekBar sbProgress;
	private Handler h = new Handler();
	private static int trackListPosition = 0;
	private ArrayList<String> trackList;
	private int mediaFileLengthInMilliseconds;
	private LoaderTask task;
	private MediaPlayer player;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player);

		creator = (TextView) findViewById(R.id.activity_player_tvCreator);
		title = (TextView) findViewById(R.id.activity_player_tvTitle);
		atime = (TextView) findViewById(R.id.activity_player_tvAllTime);
		ptime = (TextView) findViewById(R.id.activity_player_tvProgressTime);
		Typeface fontN = Typeface.createFromAsset(getAssets(), "myFontN.ttf");
		Typeface fontB = Typeface.createFromAsset(getAssets(), "myFontB.ttf");
		title.setTypeface(fontN);
		atime.setTypeface(fontN);
		ptime.setTypeface(fontN);
		creator.setTypeface(fontB);
		play = (ImageButton) findViewById(R.id.activity_player_ibPlay);
		play.setOnClickListener(this);
		prev = (ImageButton) findViewById(R.id.activity_player_ibPrev);
		prev.setOnClickListener(this);
		next = (ImageButton) findViewById(R.id.activity_player_ibNext);
		next.setOnClickListener(this);
		sbProgress = (SeekBar) findViewById(R.id.activity_player_sbProgress);
		sbProgress.setMax(99);
		sbProgress.setOnSeekBarChangeListener(this);

	}

	@Override
	protected void onResume() {
		try {
			trackList = TemporaryVariables.tracklist;
			if (player==null) {
				prepareSong(trackListPosition);
				play.setBackgroundResource(R.drawable.pause);
			} else {
				if (player.isPlaying()) { // ��� ��������� ��� ��� ����� ��� �� ������?
					releasePlayer();
					prepareSong(trackListPosition);
					play.setBackgroundResource(R.drawable.pause);
				} 
				
			}
		} catch (Exception e) {
			Log.e("myLogs", "NullPointer in PlayerActivity");
			Toast.makeText(this, getResources().getString(R.string.selectsong), Toast.LENGTH_SHORT).show();
		}
		super.onResume();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.activity_player_ibPlay:
			try {
				if (!player.isPlaying()) {
					player.start();
					seekBarProgressUpdater();
					play.setBackgroundResource(R.drawable.pause);
				} else {
					player.pause();
					play.setBackgroundResource(R.drawable.play);
				}
			} catch (Exception e) {
				Log.e("myLogs", "NullPointer in PlayerActivity in playclick");
			}
			break;
		case R.id.activity_player_ibNext:
			nextTrack();
			break;
		case R.id.activity_player_ibPrev:
			prevTrack();
			break;
		}

	}

	private void nextTrack() {
		if (player!=null) {
			releasePlayer();
			if (trackListPosition != trackList.size() - 1) {
				trackListPosition = trackListPosition + 1;
				prepareSong(trackListPosition);
				play.setBackgroundResource(R.drawable.pause);
			}
		}
	}

	private void prevTrack() {
		if (player!=null) {
		releasePlayer();
		if (trackListPosition != 0) {
			trackListPosition = trackListPosition - 1;
			prepareSong(trackListPosition);
			play.setBackgroundResource(R.drawable.pause);
		}}
	}

	private void prepareSong(int position) {
		
		initSongInfo(position);
		task = new LoaderTask(this);
		Log.d("myLogs", String.valueOf(position) + " in prepare");
		Log.d("myLogs", String.valueOf(trackList.get(position)));
		task.execute(trackList.get(position));
		try {
			player = task.get();
		} catch (InterruptedException e) {
			Log.e("myLogs", "Interrupted in PlayerActivity prepare");
		} catch (ExecutionException e) {
			Log.e("myLogs", "Execution in PlayerActivity prepare");
		}
		player.setOnBufferingUpdateListener(this);
		player.setOnCompletionListener(this);
		Log.i("myLogs", "tostring"+player.toString());
		player.start();
		mediaFileLengthInMilliseconds = player.getDuration();
		seekBarProgressUpdater();
		long allsec = player.getDuration() / 1000;
		atime.setText(timeConverter(allsec));
	}

	private void initSongInfo(int position) {
		try {
			String title = TemporaryVariables.song.get(position).get("title");
			String creator = TemporaryVariables.song.get(position).get(
					"creator");
			this.creator.setText(creator);
			this.title.setText(title);
		} catch (Exception e) {
			Log.e("myLogs", "NullPointer in PlayerActivity initsonginfo");
		}
	}

	@Override
	protected void onDestroy() {
		releasePlayer();	
		super.onDestroy();
	}

	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		if (fromUser) {
			int playPositioninMillisecconds = (mediaFileLengthInMilliseconds / 100)
					* sbProgress.getProgress();
			try {
				player.seekTo(playPositioninMillisecconds);
			} catch (NullPointerException e) {
				Log.e("myLogs", "NullPointer in PlayerActivity");
			}
	
		}
	}

	public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
		sbProgress.setSecondaryProgress(arg1);
		long allsec = arg0.getCurrentPosition() / 1000;
		ptime.setText(timeConverter(allsec));
	}

	private String timeConverter(long allsec) {
		Long mins = allsec / 60;
		Long secs = allsec - mins * 60;
		if (secs < 10) {
			return mins.toString() + ":0" + secs.toString();
		} else {
			return mins.toString() + ":" + secs.toString();
		}
	}

	public void onCompletion(MediaPlayer arg0) {
		Log.i("myLogs",
				"tracklistpos in completion: "
						+ String.valueOf(trackListPosition + 1));
		nextTrack();
	}

	private void seekBarProgressUpdater() {
		if (player!=null) {
			sbProgress
				.setProgress((int) (((float) player.getCurrentPosition() / mediaFileLengthInMilliseconds) * 100));
		if (player.isPlaying()) {
			Runnable r = new Runnable() {

				public void run() {
					seekBarProgressUpdater();
				}
			};
			h.postDelayed(r, 1000);
		}
		}
		
	}

	private void releasePlayer(){
		if (player!=null) {
			player.release();
			player = null;
		}
	}
	
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
	}


	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
	}

	public static void setTrackListPosition(int trackListPosition) {
		PlayerActivity.trackListPosition = trackListPosition;
	}

	public void onBackPressed() {
		//nothing
	}
}
